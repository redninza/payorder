--
-- Database: `payorder`
--

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` decimal(10,3) NOT NULL,
  `currency` varchar(3) NOT NULL,
  `customerFullName` varchar(150) NOT NULL,
  `cardInfo` text NOT NULL,
  `gatewayStatus` int(11) NOT NULL,
  `gatewayResponse` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
