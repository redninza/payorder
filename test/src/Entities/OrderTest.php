<?php

namespace Entities;

use Entities\Order;
use Helper\Cipher;

class OrderTest extends \PHPUnit_Framework_TestCase
{

    public function testOrderEntityValidatesWithRequiredFields()
    {
        $order = new Order();

        $order->setPrice('35.00');
        $order->setCurrency('USD');
        $order->setCustomerFullName('John Doe');

        $cardArray = array (
            'cardHolderName' => 'Bruce Wayne',
            'cardNumber'     => '4567123478903456',
            'expiryMonth'    => 6,
            'expiryYear'     => 2015,
            'cardCCV'        => 891
        );

        $cipher = new Cipher('Order-1');
        $cardInfo = $cipher->encrypt(json_encode($cardArray));

        $order->setCardInfo($cardInfo);

        $this->assertTrue($order->isValid());
    }

    public function testOrderEntityValidationFailsWithInvalidData()
    {
        $order = new Order();

        $order->setPrice('35.00');
        $order->setCurrency('BDT');
        $order->setCustomerFullName('John Doe');

        $this->assertFalse($order->isValid());
    }
}