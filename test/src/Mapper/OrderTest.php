<?php

namespace Mapper;

use Entities\Order as OrderEntity;
use Mapper\Order as OrderMapper;

class OrderTest extends \PHPUnit_Framework_TestCase
{
    /** @var OrderEntity */
    protected $orderEntity;

    /** @var OrderMapper */
    protected $orderMapper;

    public function setUp()
    {
        $this->orderEntity = new OrderEntity();
        $this->orderMapper = new OrderMapper();
    }

    public function testOrderEntityCanBeMappedFromArray()
    {
        $data = array (
            'id'                => NULL,
            'price'             => '35.5',
            'currency'          => 'USD',
            'customerFullName'  => 'John Doe',
            'cardHolderName'    => 'Bruce Wayne',
            'cardNumber'        => '4567123478903456',
            'cardExpiration'    => '2015-06-30',
            'cardCCV'           => '324',
            'status'            => 'new',
            'createdAt'         => date('Y-m-d H:i:s')
        );

        $orderMapped = $this->orderMapper->map($data, $this->orderEntity);

        $this->assertEquals($data['price'], $orderMapped->getPrice());
        $this->assertEquals($data['currency'], $orderMapped->getCurrency());
        $this->assertEquals($data['customerFullName'], $orderMapped->getCustomerFullName());
    }
}