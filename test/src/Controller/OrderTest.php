<?php

namespace Controller;

class OrderTest extends \PHPUnit_Framework_TestCase
{
    protected $endpoint;

    public function setUp()
    {
        global $config;

        $this->endpoint = $config['test_endpoint'];
        restoreDatabase();
    }

    public function testClickRecordCreatedWithValidData()
    {
        $data = array (
            'id'                => NULL,
            'price'             => '35.5',
            'currency'          => 'USD',
            'customerFullName'  => 'John Doe',
            'cardHolderName'    => 'Bruce Wayne',
            'cardNumber'        => '4567123478903456',
            'cardExpiration'    => '2015-06-30',
            'cardCCV'           => '324',
            'status'            => 'new',
            'createdAt'         => date('Y-m-d H:i:s')
        );

        $params = $this->setParams('/order', $data);
        $response = post($params);

        $responseBody = $response['body'];
        $responseCode = $response['code'];

        $orderMapped = json_decode($responseBody);

        $this->assertEquals(201, $responseCode);
        $this->assertEquals($data['price'], $orderMapped->price);
        $this->assertEquals($data['currency'], $orderMapped->currency);
        $this->assertEquals($data['customerFullName'], $orderMapped->customerFullName);
        $this->assertEquals($data['cardHolderName'], $orderMapped->cardHolderName);
        $this->assertEquals($data['cardNumber'], $orderMapped->cardNumber);
        $this->assertEquals($data['cardExpiration'], $orderMapped->cardExpiration);
        $this->assertEquals($data['cardCCV'], $orderMapped->cardCCV);
    }

    public function tearDown()
    {
       clearDatabase();
    }

    public function setParams($uri = '/', $vars = null)
    {
        $params = array();
        $params['route'] = $this->endpoint. $uri;
        $params['vars'] = $vars;

        return $params;
    }
}