<?php

namespace Repository;

use Entities\Order as OrderEntity;
use Repository\MySql\Order as OrderRepository;

class AdminTest extends BaseRepository
{
    /** @var OrderRepository */
    private $orderRepository;

    /** @var Array */
    protected $orderData;

    public function setUp()
    {
        restoreDatabase();
        $this->init();
    }

    public function init()
    {
        $this->initContainer();
        $this->prepareOrderData();
        $this->orderRepository = $this->em->getRepository('Entities\Order');
        $this->orderRepository->setContainer($this->container);
    }

    public function prepareOrderData()
    {
        $this->orderData = array (
            'id'                => NULL,
            'price'             => '35.5',
            'currency'          => 'USD',
            'customerFullName'  => 'John Doe',
            'cardHolderName'    => 'Bruce Wayne',
            'cardNumber'        => '4567123478903456',
            'expiryMonth'       => '9',
            'expiryYear'        => '2014',
            'cardCCV'           => '324',
            'status'            => 'new',
            'createdAt'         => date('Y-m-d H:i:s')
        );
    }

    public function testPlacingANewOrderWorksFine()
    {
        $order = $this->orderRepository->insert($this->orderData);

        $this->assertEquals($this->orderData['price'], $order['price']);
        $this->assertEquals($this->orderData['cardCVV'], $order['cardCVV']);
    }
}