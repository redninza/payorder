<?php

namespace Repository;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;
use Doctrine\DBAL\Connection as DBALConnection;

abstract class BaseRepository extends \PHPUnit_Framework_TestCase
{
    /** @var EntityManager */
    protected $em;

    /** @var DBALConnection; */
    protected $conn;

    protected $config;

    /** @var \Pimple */
    protected $container;

    public function setUp()
    {
        restoreDatabase();
    }

    public function initContainer()
    {
        global $container;

        $this->em     = $container['em'];
        $this->config = $container['conf'];
        $this->conn   = $container['dbal'];
        $this->container = $container;
    }

    public function tearDown()
    {
        clearDatabase();
    }

    public function setParams($uri = '/', $vars = null)
    {
        $params = array();

        $params['vars'] = $vars;
        $params['route'] = $uri;

        return $params;
    }
}