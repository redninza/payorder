<?php

use Symfony\Component\HttpFoundation\Request;

class HttpHelper
{
    public static $bootstrap;

    public static function getRequest($uri, $method = 'GET', $headers = array(), $data = array(), $query = array())
    {
        $_server = array(
            'HTTP_USER_AGENT' => 'PECL::HTTP/1.7.4 (PHP/5.3.17)',
            'HTTP_HOST' => 'hqorder.local',
            'HTTP_ACCEPT' => '*/*',
            'CONTENT_TYPE' => 'application/x-www-form-urlencoded',
            'SERVER_SIGNATURE' => '',
            'SERVER_SOFTWARE' => 'Apache/2.2.22 (Unix) DAV/2 PHP/5.3.17 mod_ssl/2.2.22 OpenSSL/0.9.8r',
            'SERVER_NAME' => 'hqorder.local',
            'SERVER_ADDR' => '127.0.0.1',
            'SERVER_PORT' => '80',
            'REMOTE_ADDR' => '127.0.0.1',
            'SERVER_ADMIN' => 'you@example.com',
            'REMOTE_PORT' => '55316',
            'GATEWAY_INTERFACE' => 'CGI/1.1',
            'SERVER_PROTOCOL' => 'HTTP/1.1',
            'REQUEST_METHOD' => $method,
            'QUERY_STRING' => '',
            'REQUEST_URI' => '/index_test.php' . $uri,
            'SCRIPT_NAME' => '/index_test.php',
            'PATH_INFO' => $uri,
            'PATH_TRANSLATED' => 'redirect:/index.php',
            'PHP_SELF' => '/index_test.php' . $uri,
            'REQUEST_TIME' => time(),
        );

        if (!empty($headers)) {
            foreach($headers as $header => $value) {
                $_server['HTTP_' . strtoupper($header)] = $value;
            }
        }

        $request = Request::create($uri, $method, $data, array(), array(), $_server);
        return $request;
    }

    public static function get($data = null)
    {
        $headers = array();
        $request = self::getRequest($data['route'], 'GET', $headers);
        return self::send($request);
    }

    public static function post($data)
    {
        $headers = array();

        $request = self::getRequest($data['route'], 'POST', $headers, $data['vars']);
        return self::send($request);
    }

    public static function put($data)
    {
        $headers = array();

        $request = self::getRequest($data['route'], 'PUT', $headers, $data['vars']);
        return self::send($request);
    }

    public static function delete($data = null)
    {
        $headers = array();

        $request = self::getRequest($data['route'], 'DELETE', $headers);
        return self::send($request);
    }

    public static function send($request)
    {
        $kernel = new AppKernel($request, self::$bootstrap->container);
        $response = $kernel->handle(true);

        $result['code'] = $response->getStatusCode();
        $result['body'] = $response->getContent();

        return $result;
    }
}

function get($data = null)
{
    $data['route'] = str_replace('http://hqorder.local/index_test.php', '', $data['route']);
    return HttpHelper::get($data);
}

function post($data)
{
    $data['route'] = str_replace('http://hqorder.local/index_test.php', '', $data['route']);
    return HttpHelper::post($data);
}

function put($data)
{
    $data['route'] = str_replace('http://hqorder.local/index_test.php', '', $data['route']);
    return HttpHelper::put($data);
}

function delete($data)
{
    $data['route'] = str_replace('http://hqorder.local/index_test.php', '', $data['route']);
    return HttpHelper::delete($data);
}