SET FOREIGN_KEY_CHECKS = 0;

--
-- Settng up table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` decimal(10,3) NOT NULL,
  `currency` varchar(3) NOT NULL,
  `customerFullName` varchar(150) NOT NULL,
  `cardInfo` text NOT NULL,
  `gatewayStatus` int(11) NOT NULL,
  `gatewayResponse` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Dumping data for table `assignment`
--

INSERT INTO `order` (`id`, `price`, `currency`, `customerFullName`, `cardInfo`, `gatewayStatus`, `gatewayResponse`, `status`, `createdAt`, `updatedAt`)
VALUES (NULL, '199.95', 'USD', 'Bruce Wayne', 'fhskjfhskjfshfkjsfmsbfsdfbskfbsdkjfbsdkjfbsdkjfbsdkjfbskfsbdfksd fsdnfskdfhsdkjfsdkjfsdkjfsdkfsdkfs', '200', 'success', 'new', '2014-05-31 09:25:44', '2014-05-31 09:25:44');


SET FOREIGN_KEY_CHECKS = 1;