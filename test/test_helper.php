<?php

function prepareBackupDatabase()
{
    global $config;

    $host = $config['mysql_test']['host'];
    $port = $config['mysql_test']['port'];
    $user = $config['mysql_test']['user'];
    $pass = $config['mysql_test']['pass'];
    $db   = $config['mysql_test']['db'];

    $script_path = __DIR__ . "/scripts/prepare.sql";

    $command = "mysql -u $user -p$pass -h $host -P $port -D $db < $script_path";
    $output = shell_exec($command);
}

function clearDatabase()
{
    global $config;

    $host = $config['mysql_test']['host'];
    $port = $config['mysql_test']['port'];
    $user = $config['mysql_test']['user'];
    $pass = $config['mysql_test']['pass'];
    $db   = $config['mysql_test']['db'];

    $script_path = __DIR__ . "/scripts/clear.sql";

    $command = "mysql -u $user -p$pass -h $host -P $port -D $db < $script_path";
    $output = shell_exec($command);
}

function restoreDatabase()
{
    global $config;

    $host = $config['mysql_test']['host'];
    $port = $config['mysql_test']['port'];
    $user = $config['mysql_test']['user'];
    $pass = $config['mysql_test']['pass'];
    $db   = $config['mysql_test']['db'];

    $script_path = __DIR__ . "/scripts/restore.sql";

    $command = "mysql -u $user -p$pass -h $host -P $port -D $db < $script_path";
    $output = shell_exec($command);
}