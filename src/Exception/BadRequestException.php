<?php

namespace Exception;

/** Thrown when an bad request is made. */
class BadRequestException extends \Exception
{

    public function __construct($msg = '')
    {
        parent::__construct(($msg) ? $msg : 'Bad Request Made', 400);
    }
}