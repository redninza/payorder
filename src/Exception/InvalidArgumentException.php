<?php

namespace Exception;

/** Thrown when an bad request is made. */
class InvalidArgumentException extends \Exception
{

    public function __construct($msg = '')
    {
        parent::__construct(($msg) ? $msg : 'One or more of the argument supplied is not valid', 400);
    }
}