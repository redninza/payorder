<?php

namespace Exception;

/** Thrown when a requested resource does not exist. */
class ResourceNotFoundException extends \Exception
{
    public function __construct($msg = '')
    {
        parent::__construct(($msg) ? $msg : 'The requested resource does not exist.', 404);
    }
}