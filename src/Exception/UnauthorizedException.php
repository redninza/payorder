<?php

namespace Exception;

/** Thrown when an unauthorized request is made. */
class UnauthorizedException extends \Exception
{

    public function __construct($errorMessage = '')
    {
        if(!$errorMessage){
            $errorMessage = 'The authorization is not valid for the requesting operation.';
        }

        parent::__construct($errorMessage, 401);
    }
}