<?php

namespace Controller;

use Symfony\Component\HttpFoundation\Response;
use Repository\MySql\Order as OrderRepository;

class Order extends Base
{
    /** @var OrderRepository */
    protected $orderRepository;

    public function init()
    {
        $this->response = new Response();
        $this->response->headers->set('Content-Type', 'application/json');

        if ($this->config['persister_choice'] == 'mysql') {

            $this->orderRepository = $this->em->getRepository('Entities\Order');

        } elseif ($this->config['persister_choice'] == 'mongodb') {

            $this->orderRepository = $this->em->getRepository('Document\Order');
        }
    }


    public function index()
    {
        $this->render('index.html.twig');
    }

    /**
     * POST /order
     *
     * @return Response
     */
    public function insert()
    {
        $this->orderRepository->setContainer($this->container);
        $this->orderRepository->setRequest($this->request);

        $data = $this->request->request->all();

        try {

            $order = $this->orderRepository->insert($data);

            $this->render('success.html.twig', $order);

        } catch (\InvalidArgumentException $e) {

            $this->render('error.html.twig');
        }
    }

    /**
     * GET /order/{id}
     *
     * @param $id
     * @return Response
     */
    public function getOrder($id)
    {
        $this->orderRepository->setContainer($this->container);
        $this->orderRepository->setRequest($this->request);

        try {

            $order = $this->orderRepository->getOrder($id);
            $this->response->setContent(json_encode(array('result' => $order, 'message' => 'success')));
            $this->response->setStatusCode(200);

        } catch (\InvalidArgumentException $e) {

            $this->response->setContent(json_encode(array('result' => '', 'message' => $e->getMessage())));
            $this->response->setStatusCode($e->getCode());
        }

        return $this->response;
    }
}