<?php

namespace Service;

use Exception\InvalidArgumentException;

class PaymentFactory
{

    public function get ($container, $paymentInfo)
    {
        $cardType = $this->_detectCardType ($paymentInfo['cardNumber']);

        if (!$cardType) {
            throw new InvalidArgumentException('Invalid card number provided');
        }

        if ($cardType == 'American Express') {
            $method = 'paypal';
        }

        if (in_array($paymentInfo['currency'], array('USD', 'EUR', 'AUD'))) {
            $method = 'paypal';
        } else {
            $method = 'braintree';
        }

        if (($cardType == 'American Express') && ($paymentInfo['currency'] != 'USD')) {
            throw new InvalidArgumentException('American Express only supports USD as currency');
        }

        $paymentInfo['cardType'] = $cardType;


        switch ($method) {

            case 'braintree':
                return new Braintree($container, $paymentInfo);

            case 'paypal':
                return new Paypal($container, $paymentInfo);

        }
    }

    private function _detectCardType ($cardNumber)
    {
        $cardNumber = str_replace(str_replace($cardNumber, " ", ""), "-", "");

        if ((strlen($cardNumber) < 14) || !is_numeric($cardNumber)) {

            return false;
        }


        switch (substr($cardNumber, 0, 2)) {

            case '34':
            case '37':
                    $cardType = "American Express";
                    break;

            case '36':
                    $cardType = "Diners Club";
                    break;

            case '38':
                    $cardType = "Carte Blanche";
                    break;

            case '51':
            case '52':
            case '53':
            case '54':
            case '55':
                    $cardType = "Master Card";
                    break;

            default:
                switch (substr($cardNumber, 0, 4)) {
                    case '2014':
                    case '2149':
                            $cardType = "EnRoute";
                            break;

                    case '2131':
                    case '1800':
                            $cardType = "JCB";
                            break;

                    case '6011':
                            $cardType = "Discover";
                            break;

                    default:
                        switch (substr($cardNumber, 0, 3)) {
                            case '300':
                            case '301':
                            case '302':
                            case '303':
                            case '304':
                            case '305':
                                    $cardType = "American Diners Club";
                                    break;

                            default:
                                switch (substr($cardNumber, 0, 3)) {
                                    case '3':
                                        $cardType = "JCB";
                                        break;

                                    case '4':
                                        $cardType = "Visa";
                                        break;

                                    default:
                                        return false;
                                }
                        }
                }
        }

        return $cardType;
    }
}