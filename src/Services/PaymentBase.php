<?php

namespace Service;

abstract class PaymentBase
{

    abstract public function __construct($container, $paymentInfo);

    abstract public function doPayment();

}