<?php

namespace Service;

use Braintree as BraintreeLib;
use Braintree_Configuration;
use Braintree_Transaction;
use Exception\BadRequestException;
use Exception\InvalidArgumentException;

class Braintree extends PaymentBase
{

    /** @var \Pimple */
    protected $container;

    /** @var Array */
    protected $paymentInfo;

    public function __construct($container, $paymentInfo)
    {
        $this->container = $container;
        $this->paymentInfo = $paymentInfo;
    }

    public function doPayment()
    {
        Braintree_Configuration::environment("sandbox");
        Braintree_Configuration::merchantId($this->container['conf']['braintree']['sandbox']['merchant_id']);
        Braintree_Configuration::publicKey($this->container['conf']['braintree']['sandbox']['merchant_id']);
        Braintree_Configuration::privateKey($this->container['conf']['braintree']['sandbox']['merchant_id']);

        $creditCard = array(
            "number"            => $this->paymentInfo['cardNumber'],
            "cvv"               => $this->paymentInfo['cardCCV'],
            "expirationMonth"   => $this->paymentInfo['expiryMonth'],
            "expirationYear"    => $this->paymentInfo['expiryYear']
        );

        $transactionAttr = array(
            "amount"            => $this->paymentInfo['price'],
            "creditCard"        => $creditCard,
            "options"           => array(
                "submitForSettlement" => true
            )
        );

        $result = Braintree_Transaction::sale($transactionAttr);


        if ($result->success) {

            return array('code' => 200, 'response' => $result->transaction->id);


        } else if ($result->transaction) {

            throw new BadRequestException($result->message);

        } else {
            $message = '';
            foreach (($result->errors->deepAll()) as $error) {
                $message .= "- " . $error->message;
            }

            throw new InvalidArgumentException($message);
        }
    }
}