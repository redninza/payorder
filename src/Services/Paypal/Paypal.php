<?php

namespace Service;

use Exception\BadRequestException;
use PayPal\Api\Amount;
use PayPal\Api\CreditCard;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\FundingInstrument;
use PayPal\Api\Transaction;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;


class Paypal extends PaymentBase
{
    /** @var \Pimple */
    protected $container;

    /** @var Array */
    protected $paymentInfo;

    public function __construct($container, $paymentInfo)
    {
        $this->container = $container;
        $this->paymentInfo = $paymentInfo;
    }

    public function doPayment()
    {
        $apiContext = $this->getApiContext();

        $card        = $this->setCreditCard();
        $fi          = $this->setFundingInstrument($card);
        $payer       = $this->setNewPayer($fi);
        $amount      = $this->setAmount();
        $transaction = $this->setTransaction($amount);
        $payment     = $this->setPayment($payer, $transaction);

        try {
            $processedPayment = $payment->create($apiContext);

        } catch (\Exception $ex) {

            throw new BadRequestException($ex->getMessage());
        }

        return array('code' => 200, 'response' => $processedPayment->toJSON());
    }

    public function getApiContext()
    {

        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                $this->container['conf']['paypal']['sandbox']['client_id'],
                $this->container['conf']['paypal']['sandbox']['secret']
            )
        );

        $apiContext->setConfig(
            array(
                'mode'                   => 'sandbox',
                'http.ConnectionTimeOut' => 30,
                'log.LogEnabled'         => false,
                'log.FileName'           => '../PayPal.log',
                'log.LogLevel'           => 'FINE'
            )
        );

        return $apiContext;
    }

    private function setCreditCard()
    {
        $card = new CreditCard();
        $card->setType($this->paymentInfo['cardType'])
            ->setNumber($this->paymentInfo['cardNumber'])
            ->setExpireMonth($this->paymentInfo['expiryMonth'])
            ->setExpireYear($this->paymentInfo['expiryYear'])
            ->setCvv2($this->paymentInfo['cardCCV'])
            ->setFirstName($this->paymentInfo['cardHolderFirstName'])
            ->setLastName($this->paymentInfo['cardHolderLastName']);

        return $card;
    }

    private function setFundingInstrument($card)
    {
        $fi = new FundingInstrument();
        $fi->setCreditCard($card);

        return $fi;
    }

    private function setNewPayer($fi)
    {
        $payer = new Payer();
        $payer->setPaymentMethod("credit_card")
        ->setFundingInstruments($fi);

        return $payer;
    }

    private function setAmount()
    {
        $amount = new Amount();
        $amount->setCurrency($this->paymentInfo['currency'])
            ->setTotal($this->paymentInfo['price']);

        return $amount;
    }

    private function setTransaction($amount)
    {
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setDescription("Payment description");

        return $transaction;
    }

    private function setPayment($payer, $transaction)
    {
        $payment = new Payment();
        $payment->setIntent("buy")
            ->setPayer($payer)
            ->setTransactions($transaction);

        return $payment;
    }
}