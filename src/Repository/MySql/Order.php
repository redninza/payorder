<?php

namespace Repository\MySql;

use Doctrine\ORM\EntityRepository;

use Entities\Order as OrderEntity;
use Exception\BadRequestException;
use Mapper\Order as OrderMapper;

use Service\PaymentFactory;

class Order extends EntityRepository
{

    /** @var OrderEntity */
    protected $orderEntity;

    /** @var \Pimple */
    protected $container;

    /** @var \Symfony\Component\HttpFoundation\Request */
    protected $request;

    /** @param \Pimple $container */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /** @param \Symfony\Component\HttpFoundation\Request $container */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * @param Array $data to insert
     * @return array inserted order array
     */
    public function insert($data)
    {
        $name = explode(' ', $data['cardHolderName']);

        $paymentInfo = array (
            'cardHolderFirstName' => $name[0],
            'cardHolderLastName'  => $name[1],
            'cardNumber'          => $data['cardNumber'],
            'expiryMonth'         => $data['expiryMonth'],
            'expiryYear'          => $data['expiryYear'],
            'cardCCV'             => $data['cardCCV'],
            'currency'            => $data['currency'],
            'price'               => $data['price'],
        );

        $paymentFactory = new PaymentFactory();
        $gateway = $paymentFactory->get($this->container, $paymentInfo);

        try {
            $gatewayResponse = $gateway->doPayment();

        } catch (\Exception $e) {

            throw new BadRequestException($e->getMessage());
        }

        $data['gatewayStatus']   = $gatewayResponse['code'];
        $data['gatewayResponse'] = $gatewayResponse['response'];

        $orderEntity = new OrderEntity();
        $orderMapper = new OrderMapper();

        $mapped = $orderMapper->map($data, $orderEntity);

        if ($mapped->isValid() === false) {
            throw new \InvalidArgumentException("Wrong data provided for the order");
        }

        $this->container['em']->persist($mapped);
        $this->container['em']->flush();

        $mapped->setCardInfo($data);
        $this->container['em']->persist($mapped);
        $this->container['em']->flush();

        return $mapped->toArray();
    }

    /**
     * @param $id
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public function getOrder($id)
    {
        $order = $this->find($id);

        if (!$order) {
            throw new \InvalidArgumentException("No such order found");
        }

        return $order->toArray();
    }
}