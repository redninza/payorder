<?php

namespace Helper;

class Cipher
{
    private $securekey;
    private $iv;

    public function __construct($textkey)
    {
        $this->securekey = hash('sha1',$textkey,TRUE);
        $this->iv = mcrypt_create_iv(32);
    }

    public function encrypt($input)
    {
        return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->securekey, $input, MCRYPT_MODE_ECB, $this->iv));
    }

    public function decrypt($input)
    {
        return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->securekey, base64_decode($input), MCRYPT_MODE_ECB, $this->iv));
    }
}