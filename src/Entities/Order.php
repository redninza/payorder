<?php

namespace Entities;

use Respect\Validation\Validator;
use Helper\Cipher;

/**
 * @Entity(repositoryClass="Repository\MySql\Order")
 * @Table(name="order")
 */

class Order
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /** @Column(type="integer", length=5) */
    protected $price;

    /** @Column(length=20) */
    protected $currency;    // [values: USD, EUR, THB, HKD, SGD, AUD]

    /** @Column(length=400) */
    protected $customerFullName;

    /** @Column(length=800) */
    protected $cardInfo;

    /** @Column(length=800) */
    protected $gatewayStatus;

    /** @Column(length=800) */
    protected $gatewayResponse;

    /** @Column(type="datetime", nullable=true) */
    protected $createdAt;

    /** @Column(type="datetime", nullable=true) */
    protected $updatedAt;

    /** @Column(length=40) */
    protected $status;          // [values: new, gateway-processed, processed, shipped, closed]

    public function setCardInfo($data)
    {
        if (!empty($data['cardHolderName'])
            && (!empty($data['cardNumber']))
            && (!empty($data['expiryMonth']))
            && (!empty($data['expiryYear']))
            && (!empty($data['cardCCV']))) {

            $cardArray = array (
                'cardHolderName' => $data['cardHolderName'],
                'cardNumber'     => $data['cardNumber'],
                'expiryMonth'    => $data['expiryMonth'],
                'expiryYear'     => $data['expiryYear'],
                'cardCCV'        => $data['cardCCV']
            );

            $cipher = new Cipher('Order-' . $this->id);
            $this->cardInfo = $cipher->encrypt(json_encode($cardArray));
        }
    }


    public function toArray()
    {
//        $cipher = new Cipher('Order-' . $this->getId());
//        $cardInfo = json_decode($cipher->decrypt($this->getCardInfo()));

        $data = array(
            'id'                => $this->getId(),
            'price'             => $this->getPrice(),
            'currency'          => $this->getCurrency(),
            'customerFullName'  => $this->getCustomerFullName(),
            'cardInfo'          => $this->getCardInfo(),
            'gatewayStatus'     => $this->getGatewayStatus(),
            'gatewayResponse'   => $this->getGatewayResponse(),
            'status'            => $this->getStatus(),
            'createdAt'         => $this->getCreatedAt(),
            'updatedAt'         => $this->getUpdatedAt(),
        );

        return $data;
    }

    public function isValid()
    {
        $currency = array('USD', 'EUR', 'THB', 'HKD', 'SGD', 'AUD');

        try {

//            Validator::create()->notEmpty()->assert($this->getCardInfo());
            Validator::create()->notEmpty()->float()->assert($this->getPrice());
            Validator::create()->notEmpty()->alnum()->in($currency)->assert($this->getCurrency());

        } catch (\InvalidArgumentException $e) {

            return false;
        }

        return true;
    }

    /** @param mixed $createdAt */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /** @return mixed */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /** @param mixed $currency */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /** @return mixed */
    public function getCurrency()
    {
        return $this->currency;
    }

    /** @param mixed $customerFullName */
    public function setCustomerFullName($customerFullName)
    {
        $this->customerFullName = $customerFullName;
    }

    /** @return mixed */
    public function getCustomerFullName()
    {
        return $this->customerFullName;
    }

    /** @param mixed $id */
    public function setId($id)
    {
        $this->id = $id;
    }

    /** @return mixed */
    public function getId()
    {
        return $this->id;
    }

    /** @param mixed $price */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /** @return mixed */
    public function getPrice()
    {
        return $this->price;
    }

    /** @param mixed $updatedAt */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /** @return mixed */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /** @param $status */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /** @return mixed */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getCardInfo()
    {
        return $this->cardInfo;
    }

    /**
     * @param mixed $gatewayResponse
     */
    public function setGatewayResponse($gatewayResponse)
    {
        $this->gatewayResponse = $gatewayResponse;
    }

    /**
     * @return mixed
     */
    public function getGatewayResponse()
    {
        return $this->gatewayResponse;
    }

    /**
     * @param mixed $gatewayStatus
     */
    public function setGatewayStatus($gatewayStatus)
    {
        $this->gatewayStatus = $gatewayStatus;
    }

    /**
     * @return mixed
     */
    public function getGatewayStatus()
    {
        return $this->gatewayStatus;
    }

}