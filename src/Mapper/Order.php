<?php

namespace Mapper;

use Entities\Order as OrderEntity;

class Order
{

    public function map($data, OrderEntity $orderEntity)
    {
        if (!empty($data['id'])) {
            $orderEntity->setId($data['id']);
        }

        if (!empty($data['price'])) {
            $orderEntity->setPrice($data['price']);
        }

        if (!empty($data['currency'])) {
            $orderEntity->setCurrency($data['currency']);
        }

        if (!empty($data['customerFullName'])) {
            $orderEntity->setCustomerFullName($data['customerFullName']);
        }

        if (!empty($data['gatewayStatus'])) {
            $orderEntity->setGatewayStatus($data['gatewayStatus']);
        }

        if (!empty($data['gatewayResponse'])) {
            $orderEntity->setGatewayResponse($data['gatewayResponse']);
        }

        if (!empty($data['createdAt'])) {
            $orderEntity->setCreatedAt(new \DateTime($data['createdAt']));
        }

        $orderEntity->setUpdatedAt(new \DateTime());

        return $orderEntity;
    }
}