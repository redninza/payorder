## THE PROBLEM
================================

### Requirements

**(1)** Create a payment gateway library, that could handle payments with:

* [Paypal REST API](https://developer.paypal.com/docs/api/)
* [Braintree payments](https://www.braintreepayments.com/docs/php/guide/overview)

Library should be designed to easily add another additional payment gateways.
.
.
.
**(2)** Create a simple form for making payment. Form should have this fields:

* In order section:
  * Price (*amount*)
  * Currency (*USD, EUR, THB, HKD, SGD, AUD*)
  * Customer Full name
* In payment section:
  * Credit card holder name
  * Credit card number
  * Credit card expiration
  * Credit card CCV
* Submit button

Show success or error message after payment. Use appropriate form validations.
.
.
.
.

**(3)** Save order data + response from payment gateway to database table.
.
.
.

### Specification

* Create sandbox accounts for Paypal and Braintree

* After submitting the form, use a different gateway based on these rules:
  * if credit card type is AMEX, then use Paypal.
  * if currency is USD, EUR, or AUD, then use Paypal. Otherwise use Braintree.
  * if currency is **not** USD and credit card **is** AMEX, return error message, that AMEX is possible to use only for USD
  
* Use any PHP framework you want or no framework at all, it's up to you.
*
* Don't bother with any graphics, just simple HTML, simple form, no CSS needed. Or just use [Twitter Bootstrap](http://getbootstrap.com).

* Use only Paypal and Braintree PHP libraries, not any other 3rd party libraries.

* Cover code with unit tests.

* The code needs to work after we pull it and try it (no bugs) and should process the payments.
.
.
.

## SOLUTION
=================================================

### INSTALLATION

 - Create database named "*payorder*"

 - Run the *Schema/order.sql* 

 - Run ```composer install``` to install the vendors

 - Change if any change is required in ```app/config/config.yml``` and/or ```app/config/parameter.yml```

 - give permission the the mentioned folders
```sudo chmod -R 0777 app/cache/ app/logs/ src/View/cache/```



### RUNNING:

 - To run the app point your browser to ```"http://localhost/PayOrder/web/"``` or wherever you have stored the files.

### TESTING:

 - I have introduced unit testing in some of the features using PHPUnit.
 - To run a test (or all tests) please try following commands:
 ```
cd test
phpunit src/Entities/.
```